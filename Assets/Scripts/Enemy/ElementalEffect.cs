using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementalEffect : LocalSingleton<ElementalEffect>
{
    public void Fire(ParticleSystem fire)
    {
        fire.Play();
    }
    public void Ice(GameObject �ce)
    {
        �ce.SetActive(true);
    }
    public void Electricity(ParticleSystem electricity)
    {
        electricity.Play();
    }
    public void Break(ParticleSystem breakEffect)
    {
        breakEffect.Play();
    }
}
