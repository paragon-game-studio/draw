using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Animancer;

public class Enemy : LocalSingleton<Enemy>
{
    Rigidbody[] rb;
    public GameObject happyMesh;
    public GameObject cryMesh;
    public GameObject cryMesh2;
    public GameObject deadMesh;
    [HideInInspector]
    public AnimancerComponent enemyAnimancer;
    public GameObject player;
    public float speed = 0.0006f;
    public float force = 1;
    [HideInInspector] public bool playerTrigger = false;
    bool first = false;
    bool �ceSkill = false;
    public bool isDead = false;
    [SerializeField] private ParticleSystem fire, electricity, breakEffect;
    [SerializeField] private GameObject �ce;
    [SerializeField] private GameObject stick;

    private void Start()
    {
        rb = GetComponentsInChildren<Rigidbody>();
        enemyAnimancer = GetComponent<AnimancerComponent>();
        AnimationManager.Instance.EnemyIdle(enemyAnimancer);
        Ragdoll(rb, true);
    }
    void Update()
    {
        // transform.DOMove(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z), speed);
        if (playerTrigger)
        {
            transform.position = Vector3.Lerp(transform.position, new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z), speed);
            if (!first)
            {
                first = true;
                AnimationManager.Instance.EnemyWalk(enemyAnimancer);
            }

        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Attack"))
        {
            GetComponent<Collider>().enabled = false;
            DeadMaterial();
            if (other.gameObject.layer == 10)
            {
                //ElementalEffect.Instance.Electricity(electricity);
                player = other.gameObject;
                speed = 0.01f;
                Ragdoll(rb, false);
                Destroy(gameObject, 3.5f);
                stick.GetComponent<Rigidbody>().isKinematic = false;
                Invoke("RagdollPortal", .9f);
            }
            else if (other.gameObject.layer == 8)
            {
                �ceSkill = true;
                Force();
                ElementalEffect.Instance.Ice(�ce);
            }
            else if (other.gameObject.layer == 7)
            {
                ElementalEffect.Instance.Electricity(electricity);
                stick.GetComponent<Rigidbody>().isKinematic = false;
                Force();
            }
            else if (other.gameObject.layer == 11)
            {
                ElementalEffect.Instance.Fire(fire);
                stick.GetComponent<Rigidbody>().isKinematic = false;
                Force();
            }
        }

    }
    void Force()
    {
        playerTrigger = false;
    }
    void DeadMaterial()
    {
        happyMesh.SetActive(false);
        cryMesh.SetActive(true);
        cryMesh2.SetActive(true);
        EnemyGroup.Instance.enemyList.Remove(gameObject);
        EnemyGroup.Instance.PlayerLook();
        AnimationManager.Instance.EnemyDamage(enemyAnimancer);
        Invoke("Dead", 1f);
    }
    void Dead()
    {
        cryMesh2.SetActive(false);
        cryMesh.SetActive(false);
        deadMesh.SetActive(true);
        stick.transform.SetParent(null);

        if (�ceSkill)
        {
            gameObject.GetComponent<Animator>().enabled = false;
            �ceSkill = false;
        }
        else
        {
            Ragdoll(rb, false);
        }

    }
    void Ragdoll(Rigidbody[] rb, bool valuve)
    {
        gameObject.GetComponent<Animator>().enabled = valuve;
        foreach (Rigidbody chieldRB in rb)
        {
            chieldRB.isKinematic = valuve;
        }
    }
    void RagdollPortal()
    {
        Collider[] col = GetComponentsInChildren<Collider>();
        foreach (Collider chieldRB in col)
        {
            chieldRB.isTrigger = true;
        }
    }
}
