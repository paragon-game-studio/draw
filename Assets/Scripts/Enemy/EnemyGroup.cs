using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EnemyGroup : LocalSingleton<EnemyGroup>
{
    public List<GameObject> enemyList;
    public GameObject player;
    int move;
    int look;
    public int maxEnemy = 3;
    void Start()
    {
        Play();
    }
    public void Play()
    {
        PlayerLook();
        for (int i = 0; i < enemyList.Count; i++)
        {
            enemyList[i].GetComponent<Enemy>().playerTrigger = true;
        }
    }
    public void PlayerLook()
    {
        //Skills.Instance.target = enemyList[look];
        //Vector3 rot = new Vector3(enemyList[look].transform.position.x, enemyList[look].transform.position.y, enemyList[look].transform.position.z);
        //rot.y = PathMove.Instance.lookX.transform.position.y;
        //player.transform.DOLookAt(rot, 1f).SetDelay(.8f);
        //if (look < enemyList.Count - 1)
        //{
        //    if (enemyList[look].GetComponent<Enemy>().isDead)
        //    {
        //        PlayerLook();
        //        look--;
        //    }
        //    else
        //    {
        //        look++;
        //    }

        //}
        if (enemyList.Count > 0)
        {
            Skills.Instance.target = enemyList[0];
            Vector3 rot = new Vector3(enemyList[0].transform.position.x, enemyList[0].transform.position.y, enemyList[0].transform.position.z);
            rot.y = PathMove.Instance.lookX.transform.position.y;
            player.transform.DOLookAt(rot, 1f).SetDelay(.8f);
        }
        EnemyPathMove();

    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    if (other.CompareTag("Enemy"))
    //    {
    //        enemyList.Add(other.gameObject);
    //        if (enemyList.Count == maxEnemy)
    //        {
    //            Play();
    //        }
    //    }
    //}
    public void EnemyPathMove()
    {
        if (enemyList.Count == 0)
        {
            StartCoroutine(Follow());
        }

    }
    IEnumerator Follow()
    {
        yield return new WaitForSeconds(1);
        move = 0;
        look = 0;
        PathMove.Instance.PathFollow();
    }
}
