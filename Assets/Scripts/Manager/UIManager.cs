using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using SupersonicWisdomSDK;

public class UIManager : LocalSingleton<UIManager>
{
    public UI uı;

    private int level;

    public int Level
    {
        get { return PlayerPrefs.GetInt("sLevel", level); }
        set {
        
            PlayerPrefs.SetInt("sLevel", value);
            level = value; 
        }
    }

    private static int _currlevel=1; 
    public static int currLevel
    {
        get
        {
            return PlayerPrefs.GetInt("currLevel", _currlevel);
        }
        set
        { 
            PlayerPrefs.SetInt("currLevel", value);
            _currlevel = value;
        }
    }
    
    
    public void TapToStart()
    {
       uı.tapToStart.transform.DOScale(new Vector3(0, 0, 0), .5f).SetEase(Ease.InBack).OnComplete(()=> 
        {

            GameManager.Instance.playGame = true;
            GameManager.Instance.GroupPlay();
            uı.drawPanel.SetActive(true);
            uı.skillPanel.SetActive(true);
            uı.tapToStart.SetActive(false);
            SupersonicWisdom.Api.NotifyLevelStarted(Level, null);
        });
        
    }
    public void Failed()
    {
        uı.failPanel.transform.DOScale(Vector3.one, .5f).SetEase(Ease.OutBack);
        uı.drawPanel.SetActive(false);
        uı.skillPanel.SetActive(false);
        SupersonicWisdom.Api.NotifyLevelFailed(Level, null);
    }
    public void Finish()
    {
        uı.finishPanel.transform.DOScale(Vector3.one, .5f).SetEase(Ease.OutBack);
        uı.drawPanel.SetActive(false);
        uı.skillPanel.SetActive(false);
        SupersonicWisdom.Api.NotifyLevelCompleted(Level, null);
    }
    public void ResetButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void DrawL()
    {
        uı.drawL.transform.DOPunchScale(new Vector3(0.3f, 0.3f, 0.3f), .5f, 1, 1).OnComplete(() =>
           {
               uı.drawL.SetActive(false);
               uı.fadeL.SetActive(true);
           });
        
    }    
    public void DrawO()
    {
        uı.drawO.transform.DOPunchScale(new Vector3(0.3f, 0.3f, 0.3f), .5f, 1, 1).OnComplete(() =>
           {
               uı.drawO.SetActive(false);
               uı.fadeO.SetActive(true);
           });
        
    }  
    public void DrawU()
    {
        uı.drawU.transform.DOPunchScale(new Vector3(0.3f, 0.3f, 0.3f), .5f, 1, 1).OnComplete(() =>
           {
               uı.drawU.SetActive(false);
               uı.fadeU.SetActive(true);
           });
        
    }

    public void NextButton()
    {
        _currlevel++;
        Level++;
        SceneManager.LoadScene("Level" + (((currLevel - 1) % SDKManager.maxLevelCount) + 1));
    }


    public void Restart()
    {
        uı.drawL.SetActive(true);
        uı.drawO.SetActive(true);
        uı.drawU.SetActive(true);

        uı.fadeU.SetActive(false);
        uı.fadeL.SetActive(false);
        uı.fadeO.SetActive(false);
    }
}
