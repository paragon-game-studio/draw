using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;
[System.Serializable]
public class Animations 
{
    public AnimancerComponent playerAnimancer;
    public AnimationClip clipU;
    public AnimationClip clipC;
    public AnimationClip clipO;
    public AnimationClip clipL;
    public AnimationClip attack;
    public AnimationClip idle;
    //-------------Enemy--------------------
    public AnimationClip enemyIdle;
    public AnimationClip enemyWalk;
    public AnimationClip damage;
}
