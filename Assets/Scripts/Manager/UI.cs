using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UI 
{
    public GameObject drawL, drawO, drawU;
    public GameObject fadeL, fadeO, fadeU;
    public GameObject tapToStart, skillPanel, drawPanel;
    public GameObject failPanel;
    public GameObject finishPanel;



}
