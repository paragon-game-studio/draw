﻿
using SupersonicWisdomSDK;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SDKManager : MonoBehaviour
{
    public static int maxLevelCount = 4;

    void Awake()
    {
        // Subscribe
        SupersonicWisdom.Api.AddOnReadyListener(OnSupersonicWisdomReady);
        // Then initialize
        SupersonicWisdom.Api.Initialize();
    }

    void OnSupersonicWisdomReady()
    {
        SceneManager.LoadScene("Level" + (((UIManager.currLevel - 1) % maxLevelCount) + 1));
    }
   

}
