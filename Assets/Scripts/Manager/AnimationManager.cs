using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class AnimationManager : LocalSingleton<AnimationManager>
{
    [SerializeField] private Animations animation;

    public float time =2;
    private void Start()
    {
        PlayAnim(animation.idle);
    }
    public void PlayAnim(AnimationClip clip, float speed = 1f, float fade = .25f)
    {
        var state = animation.playerAnimancer.Play(clip, fade);
        state.Speed = speed;

        if (clip == animation.attack)
        {
            StartCoroutine(PlayIdle(time));
        }

    }

    public void PlayAttack()
    {
        PlayAnim(animation.attack, 3);
        ParticleSystemPP();
    }
    public void PlayU()
    {
        PlayAnim(animation.clipU, 3);
    }
    public void PlayL()
    {
        PlayAnim(animation.clipL, 3);
    }
    public void PlayO()
    {
        PlayAnim(animation.clipO, 3);
    }
    IEnumerator PlayIdle(float time)
    {
        ParticleSystemPP();
        yield return new WaitForSeconds(time);
        PlayAnim(animation.idle);
    }
    void ParticleSystemPP()
    {
        SkillDraw.Instance.magicWorld.Stop();
    }
    public void EnemyIdle(AnimancerComponent animancer)
    {
        var state = animancer.Play(animation.enemyIdle, .3f);
        state.Speed = 1;
    }
    public void EnemyWalk(AnimancerComponent animancer)
    {
        var state = animancer.Play(animation.enemyWalk, .3f);
        state.Speed = .3f;
    }
    public void EnemyDamage(AnimancerComponent animancer)
    {
        var state = animancer.Play(animation.damage, .3f);
        state.Speed = .3f;
    }
}
