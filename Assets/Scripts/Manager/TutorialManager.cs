using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Animancer;

public class TutorialManager : LocalSingleton<TutorialManager>
{
    public GameObject hand, tutorialO, tutorialU, tutorialL;

    public AnimancerComponent handAnimancer;
    public AnimationClip clipO, clipL, clipU;


    public bool drawO = false, drawL = false, drawU = false, drawTwo = false, draw = true;

    public int tutorialIndex;
    private void Update()
    {
        if (GameManager.Instance.playGame && !drawTwo)
        {
            if (!drawL || !drawO || !drawU)
                hand.SetActive(true);
            else
                hand.SetActive(false);
            if (!drawO)
            {
                TutorialO();
            }
            else if (!drawU)
            {
                TutorialU();
            }
            else if (!drawL)
            {
                TutorialL();
            }
            else
            {
                tutorialL.SetActive(false);
                tutorialO.SetActive(false);
                tutorialU.SetActive(false);
                
            }
        }
        else if (drawTwo && draw)
        {
            if (drawL || drawO || drawU)
                hand.SetActive(true);
            else
                hand.SetActive(false);
            if (drawO)
            {
                TutorialO();
            }
            else if (drawL)
            {
                TutorialL();
            }
            else if (drawU)
            {
                TutorialU();
            }
            if (tutorialIndex == 6)
            {
                tutorialO.SetActive(false);
                tutorialU.SetActive(false);
                tutorialL.SetActive(false);
                enabled = false;
            }
        }
       
    }

    public void TutorialO()
    {
        tutorialO.SetActive(true);
        tutorialU.SetActive(false);
        tutorialL.SetActive(false);
        var state = handAnimancer.Play(clipO, .25f);
        state.Speed = 1;
    }
    public void TutorialL()
    {
        tutorialL.SetActive(true);
        tutorialU.SetActive(false);
        tutorialO.SetActive(false);
        var state = handAnimancer.Play(clipL, .25f);
        state.Speed = 1;
    }
    public void TutorialU()
    {
        tutorialU.SetActive(true);
        tutorialO.SetActive(false);
        tutorialL.SetActive(false);
        var state = handAnimancer.Play(clipU, .25f);
        state.Speed = 1;
    }
}
