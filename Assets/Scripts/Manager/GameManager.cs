using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : LocalSingleton<GameManager>
{
    [SerializeField] private GameObject[] enemyGroup;
    public int lvlEnemyGroup;

    public bool playGame = false;

    public bool tutorial = true;
    public int tutorialPrefs = 0;
    private void Start()
    {
        tutorialPrefs = PlayerPrefs.GetInt("Tutorial");
        if (tutorialPrefs == 0)
        {
            tutorialPrefs++;
            TutorialManager.Instance.enabled = true;
        }
        else
        {
            TutorialManager.Instance.enabled = false;
            TutorialManager.Instance.drawL = true;
            TutorialManager.Instance.drawO = true;
            TutorialManager.Instance.drawU = true;
        }
        PlayerPrefs.SetInt("Tutorial",tutorialPrefs);
    }
    public void GroupPlay()
    {

        if (playGame)
        {
            if (PathMove.Instance.pathIndex > 0)
            {
                Destroy(enemyGroup[PathMove.Instance.pathIndex - 1], .3f);

            }

            if (PathMove.Instance.pathIndex < lvlEnemyGroup)
            {
                enemyGroup[PathMove.Instance.pathIndex].SetActive(true);
            }
            
        }
       
    }
}
