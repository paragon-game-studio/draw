using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgsaaMaterial : LocalSingleton<AgsaaMaterial>
{
    

    [SerializeField] private GameObject fire, ice, portal, light, bang;
    public void IceMaterial()
    {
        OpenMaterial(value1: true);
    }
    public void FireMaterial()
    {
        OpenMaterial(value4: true);
    }
    public void PortalMaterial()
    {
        OpenMaterial(value3: true);
    } 
    public void LightMaterial()
    {
        OpenMaterial(value2: true);
    } 
    public void BangMaterial()
    {
        OpenMaterial(value5:true);
    }
    void OpenMaterial(bool value1 = false, bool value2 = false, bool value3 = false, bool value4 = false, bool value5 = false)
    {
        ice.SetActive(value1);
        light.SetActive(value2);
        portal.SetActive(value3);
        fire.SetActive(value4);
        bang.SetActive(value5);
    }
}
