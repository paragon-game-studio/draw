using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightningParticle : LocalSingleton<LightningParticle>
{
    [SerializeField] private GameObject lightning;
   
    private void OnEnable()
    {
        GetComponent<LightningParticle>().enabled = false;
        StartCoroutine(Play());
    }
    IEnumerator Play()
    {
        yield return new WaitForSeconds(.9f);
        for (int i = 0; i < 30; i++)
        {
            GameObject a = Instantiate(lightning, transform.position + (new Vector3(Random.Range(-2, 2), 0, Random.Range(-1,1))), Quaternion.identity);
            yield return new WaitForSeconds(.2f);
            Destroy(a, 2);
        }
    }
}
