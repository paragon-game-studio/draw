using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destroyer : MonoBehaviour
{
    public ParticleSystem bang;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Floor"))
        {
            bang.Play();
            GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}
