using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Skills : LocalSingleton<Skills>
{
    [SerializeField] private SkillParticles particle;
    public float particlePlay;

    [SerializeField] private GameObject ıceSkillTransform, diamondSkillTransform, fire, portal, electricitySkillTransform, bangSkill;
    [HideInInspector]
    public GameObject target;
    public GameObject skillObj;
    public bool skillPlayed;

    [SerializeField] private GameObject effectIce, effectDiamond, effectPortal, effectLight, startPos;
    public void PlaySkill(string Draw1, string Draw2, string Draw3)
    {
        if (Draw1 == "U" && Draw2 == "Yuvarlak" && Draw3 == "L")
        {
            Attack();
            Invoke("PlayIce", particlePlay);
        }
        else if (Draw1 == "L" && Draw2 == "Yuvarlak" && Draw3 == "U")
        {
            Invoke("PlayDiamond", particlePlay);
            Attack();
        }
        else if (Draw1 == "Yuvarlak" && Draw2 == "U" && Draw3 == "L")
        {
            Invoke("PlayLight", particlePlay);
            Attack();
        }
        else if (Draw1 == "L" && Draw2 == "U" && Draw3 == "Yuvarlak")
        {
            Invoke("PlayPortal", particlePlay);
            Attack();
        }
        else if (Draw1 == "U" && Draw2 == "L" && Draw3 == "Yuvarlak")
        {
            Invoke("PlayFire", particlePlay);
            Attack();
        }
        else if (Draw1 == "Yuvarlak" && Draw2 == "L" && Draw3 == "U")
        {
            Invoke("PlayBang", particlePlay);
            Attack();
        }
        
        Draw1 = "";
        Draw2 = "";
        Draw3 = "";
    }
    void Attack()
    {
        AnimationManager.Instance.PlayAttack();
    }

    void PlayIce()
    {
        AgsaaMaterial.Instance.IceMaterial();
        ıceSkillTransform.transform.position = target.transform.position;
        effectIce.transform.DOLookAt(ıceSkillTransform.transform.position, .0f).SetDelay(.3f).OnComplete(() => { particle.ıceParticle.Play(); });
        StartCoroutine(ColliderOn(ıceSkillTransform, .7f));


    }
    void PlayLight()
    {
        AgsaaMaterial.Instance.LightMaterial();
        electricitySkillTransform.transform.position = target.transform.position;
        particle.lightningParticle.GetComponent<LightningParticle>().enabled = true;
        effectLight.transform.DOLookAt(electricitySkillTransform.transform.position, .0f).SetDelay(.3f).OnComplete(()=> { effectLight.SetActive(true); });
        electricitySkillTransform.transform.position = new Vector3(target.transform.position.x, electricitySkillTransform.transform.position.y+4, target.transform.position.z);
        StartCoroutine(ColliderOn(null, 1));
    }
    void PlayPortal()
    {
        AgsaaMaterial.Instance.PortalMaterial();
        portal.transform.SetParent(null);
        portal.transform.position = target.transform.position;
        portal.transform.DORotate(new Vector3(90, 0, 0), 0);
        effectPortal.transform.DOLookAt(portal.transform.position, .0f).SetDelay(.3f).OnComplete(() => { effectPortal.SetActive(true); });
        particle.portalParticle.Play();
        StartCoroutine(ColliderOn(portal, .7f));
        
    }
    void PlayFire()
    {
        AgsaaMaterial.Instance.FireMaterial();
        particle.fireParticle.Play();
        StartCoroutine(ColliderOn(fire, 1f));

    }
    void PlayDiamond()
    {
        AgsaaMaterial.Instance.LightMaterial();
        particle.diamondParticle.Play();
        diamondSkillTransform.transform.position = target.transform.position;
        StartCoroutine(ColliderOn(diamondSkillTransform, .7f));

        effectDiamond.transform.position = startPos.transform.position;
        effectDiamond.SetActive(true);
        effectDiamond.transform.DOMove(diamondSkillTransform.transform.position, 1f);
    }
    void PlayBang()
    {
        AgsaaMaterial.Instance.BangMaterial();
        particle.bangParticle.Play();
        StartCoroutine(ColliderOn(bangSkill, 2.4f));
    }

    IEnumerator ColliderOn(GameObject obj, float time)
    {
        skillPlayed = true;
        SkillDraw.Instance.skillPlayed = true;
        yield return new WaitForSeconds(time);
        if (obj!= null)
            obj.GetComponent<Collider>().enabled = true;
        yield return new WaitForSeconds(.1f);
        Restart(obj);
        yield return new WaitForSeconds(time * 5);
        if (obj != fire && obj != null)
        {
            obj.transform.SetParent(skillObj.transform);
        }
    }
    void Restart(GameObject obj)
    {
        SkillDraw.Instance.skillPlayed = false;
        if (obj != null)
            obj.GetComponent<Collider>().enabled = false;
        skillPlayed = false;
    }

}
