using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SkillParticles
{
    public ParticleSystem ęceParticle;
    public ParticleSystem fireParticle;
    public ParticleSystem portalParticle;
    public ParticleSystem diamondParticle;
    public ParticleSystem bangParticle;
    public GameObject lightningParticle;
}
