using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diamond : MonoBehaviour
{
    public float rotSpeed;
    void FixedUpdate()
    {
        transform.Rotate(rotSpeed* Time.deltaTime, rotSpeed* Time.deltaTime, rotSpeed* Time.deltaTime);
    }
}
