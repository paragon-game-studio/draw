using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PathMove : LocalSingleton<PathMove>
{
    public PathList[] pathList;
    public GameObject lookX;
    [SerializeField] private float speed;
    public int pathIndex;
    int step = 0;

    public bool moved = false;


    private void Start()
    {
        GameManager.Instance.GroupPlay();
    }
    public void PathFollow()
    {
        StartCoroutine(PathSmoothnes());
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("EnemyGroup"))
        {
            if (GameManager.Instance.tutorial)
            {
                TutorialManager.Instance.drawTwo = !TutorialManager.Instance.drawTwo;
            }
            moved = false;
            UIManager.Instance.u�.skillPanel.SetActive(true);
            other.gameObject.GetComponent<EnemyGroup>().enabled = true;
            LightningParticle.Instance.gameObject.SetActive(true);
            //GameObject a = other.gameObject.GetComponent<EnemyGroup>().enemyList[0];
            //Skills.Instance.target = a;

        }
        else if (other.CompareTag("Enemy") && !Skills.Instance.skillPlayed)
        {
            UIManager.Instance.Failed();

        }
        else if (other.CompareTag("Finish"))
        {
            UIManager.Instance.Finish();

        }
    }
    IEnumerator PathSmoothnes()
    {
        moved = true;
        UIManager.Instance.u�.skillPanel.SetActive(false);
        LightningParticle.Instance.gameObject.SetActive(false);
        for (int i = 0; i < pathList[step].path.Length; i++)
        {
            transform.DOLookAt(pathList[step].lookObj.transform.position, 1);
            transform.DOMove(pathList[step].path[i].transform.position, speed);
            yield return new WaitForSeconds(speed);
        }
        pathIndex++;
        GameManager.Instance.GroupPlay();
        step++;
    }
}
[System.Serializable]
public class PathList
{
    public GameObject[] path;
    public GameObject lookObj;
}
