using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DrawCam
{
    public GameObject drawObject;
    public GameObject drawPrefab;

    public void Follow()
    {
        Ray mousRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        drawObject.transform.position = mousRay.GetPoint(10f);
    }


}
