using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillDraw : LocalSingleton<SkillDraw>
{
    public List<int> drawPoint;
    [HideInInspector]
    public int drawIndex = 0;
    bool firstTap;

    [HideInInspector] public List<string> skillDrawList;
    int skillDrawListIndex = 0;

    bool L = true;
    bool O = true;
    bool U = true;
    [HideInInspector] public bool skillPlayed = false;
    public ParticleSystem magicWorld;
    private void Start()
    {
        drawPoint.Add(0);
        drawPoint.Add(0);
        drawPoint.Add(0);
        drawPoint.Add(0);
        drawPoint.Add(0);
        skillDrawList.Add("");
        skillDrawList.Add("");
        skillDrawList.Add("");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (!PathMove.Instance.moved)
        {
            if (other.CompareTag("DrawControl-1"))
            {
                StartPoint(0);
                drawPoint[drawIndex] = 1;
            }
            else if (other.CompareTag("DrawControl-2"))
            {
                StartPoint(1);
                drawPoint[drawIndex] = 2;
            }
            else if (other.CompareTag("DrawControl-3"))
            {
                StartPoint(2);
                drawPoint[drawIndex] = 3;
            }
            else if (other.CompareTag("DrawControl-4"))
            {
                StartPoint(3);
                drawPoint[drawIndex] = 4;
            }
            if (drawIndex < 4)
            {
                drawIndex++;
            }
        }
        else
        {
            Restart();
        }


    }
    void StartPoint(int start)
    {
        firstTap = true;
        if (drawPoint[start] == 1 && !firstTap)
        {
            drawPoint[drawIndex] = start;
        }
    }
    void ParticleSystemPP()
    {
        magicWorld.Play();
    }
    public void SkillPlay()
    {
        if (!skillPlayed)
        {
            if (drawPoint[0] >= 1 && drawPoint[1] >= 1 && drawPoint[2] >= 1 && drawPoint[3] == 0 && drawPoint[4] == 0 && L)
            {   // L
                L = false;
                if (GameManager.Instance.tutorial)
                {
                    TutorialManager.Instance.drawL = !TutorialManager.Instance.drawL;
                    TutorialManager.Instance.tutorialIndex++;
                }
                ParticleSystemPP();
                UIManager.Instance.DrawL();
                AnimationManager.Instance.PlayL();
                skillDrawList[skillDrawListIndex] = "L";
            }
            else if (drawPoint[0] >= 1 && drawPoint[1] >= 1 && drawPoint[2] >= 1 && drawPoint[3] >= 1 && drawPoint[4] == 0 && U)
            {   // Sa� U
                U = false;
                if (GameManager.Instance.tutorial)
                {
                    TutorialManager.Instance.drawU = !TutorialManager.Instance.drawU;
                    TutorialManager.Instance.tutorialIndex++;
                } 
                ParticleSystemPP();
                UIManager.Instance.DrawU();
                AnimationManager.Instance.PlayU();
                skillDrawList[skillDrawListIndex] = "U";
            }
            else if (drawPoint[0] >= 1 && drawPoint[1] >= 1 && drawPoint[2] >= 1 && drawPoint[3] >= 1 && drawPoint[4] >= 1 && O)
            {   //Yuvarlak
                O = false;
                if (GameManager.Instance.tutorial)
                {
                    TutorialManager.Instance.drawO = !TutorialManager.Instance.drawO;
                    TutorialManager.Instance.tutorialIndex++;
                }
                ParticleSystemPP();
                UIManager.Instance.DrawO();
                AnimationManager.Instance.PlayO();
                skillDrawList[skillDrawListIndex] = "Yuvarlak";
            }
            else
            {
                Restart();
            }
            if (skillDrawList[skillDrawListIndex] != "")
            {
                skillDrawListIndex++;
                Invoke("Restart", .2f);
            }
        }
    }
    void DrawRestart()
    {
        skillPlayed = true;
        Skills.Instance.PlaySkill(skillDrawList[0], skillDrawList[1], skillDrawList[2]);
        skillDrawListIndex = 0;
        for (int i = 0; i < skillDrawList.Count; i++)
        {
            skillDrawList[i] = "";
        }
        UIManager.Instance.Restart();
        L = true;
        U = true;
        O = true;
    }
    void Restart()
    {
        if (skillDrawListIndex == 3)
        {
            Invoke("DrawRestart", 1.2f);
        }
        drawIndex = 0;
        gameObject.GetComponent<Collider>().enabled = false;
        for (int i = 0; i < drawPoint.Count; i++)
        {
            drawPoint[i] = 0;
        }
    }
}
