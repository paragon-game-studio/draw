using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DrawManager : MonoBehaviour
{
    public DrawCam drawCam;
    GameObject draw;
    public Material drawMaterial;
    public Material drawMaterialFade;
    public GameObject player;
    public Vector3 offset;

    float a;
    private void Start()
    {
        SkillDraw.Instance.gameObject.GetComponent<Collider>().enabled = false;
    }
    void LateUpdate()
    {
        transform.DOMove(player.transform.position+offset, 0);
        drawCam.Follow();
        if (!PathMove.Instance.moved)
        {
            if (Input.GetMouseButtonDown(0))
            {
                draw = Instantiate(drawCam.drawPrefab, drawCam.drawObject.transform.position, Quaternion.identity);
                draw.GetComponent<TrailRenderer>().material = drawMaterial;
                draw.transform.SetParent(gameObject.transform);
                SkillDraw.Instance.gameObject.GetComponent<Collider>().enabled = true;
            }
            else if (Input.GetMouseButton(0))
            {
                if (draw.transform.localPosition.y > -2.9f || draw.transform.localPosition.y < -4.7f || draw.transform.localPosition.x < -1.9f || draw.transform.localPosition.x > 1.9f)
                {
                    if (draw == null)
                    {
                        return;
                    }
                    draw.GetComponent<TrailRenderer>().enabled = false;
                    SkillDraw.Instance.GetComponent<Collider>().enabled = false;
                }
                else
                {
                    Ray mousRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                    draw.transform.position = mousRay.GetPoint(10f);
                }
            }
            else if (Input.GetMouseButtonUp(0))
            {
                if (draw == null)
                {
                    return;
                }
                SkillDraw.Instance.gameObject.GetComponent<Collider>().enabled = false;
                SkillDraw.Instance.SkillPlay();
                draw.GetComponent<TrailRenderer>().material = drawMaterialFade;
                drawMaterialFade.color = new Color(0, 0, 0, 1f);
                a = 1;
                Destroy(draw, .8f);
            }
            if (a <= 1)
            {
                a -= .015f;
                drawMaterialFade.color -= new Color(0, 0, 0, .015f);
            }
        }
        else if (draw != null && PathMove.Instance.moved)
        {
            draw.SetActive(false);
        }
        
        
    }
}
