using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barrel : MonoBehaviour
{
    public ParticleSystem barrelBang;
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Attack"))
        {
            barrelBang.gameObject.transform.SetParent(null);
            barrelBang.Play();
            gameObject.tag = "Attack";
            gameObject.SetActive(false);
            gameObject.GetComponent<Collider>().enabled = false;
        }
    }
}
